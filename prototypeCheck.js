/**
 * @param {any} obj
 * @param {any} classFunction
 * @return {boolean}
 */
 
var checkIfInstanceOf = function (obj, classFunction) {
    if (obj === null || typeof obj === 'undefined' || classFunction === null) {
        return false;
    }

    let proto = Object.getPrototypeOf(obj);
    
    while (proto) {
        if(proto.constructor === classFunction)
            return true;
        
        proto = Object.getPrototypeOf(proto);
    }
    return false;
};


/**
 * checkIfInstanceOf(new Date(), Date); // true
 */